# Fellowship Voting Power Scenario

I found the Polkadot Fellowship interesting to analyze
since it might become very important for Polkadot over
time, and it aims for political decentralisation.

I wanted to project its decentralisation.

My analysis is based on these sources:

 1. [The Forum Thread](https://forum.polkadot.network/t/calling-polkadot-core-developers/506)
 2. [The Manifesto](https://github.com/polkadot-fellows/manifesto/blob/3b172c22914336adec4d7df0ece2890500125910/manifesto.pdf)

## Background

Polkadot is forming a new governance structure.
The Fellowship is given small but special powers within this new structure.
It will get to speed up individual referendums.

It takes over from the Technical Committee and is claimed to be substantially more (politically) decentralised,
since it will have more members than the Technical Committee had (which was ~3).

I believe the Fellowship's role might become more important in the future than the Technical Committee's was in the past.
Particularly if the wider community starts regarding it as a proper, authentic, meritocratic,
and decentralised group of independent subject matter experts.

## Decentralised?

The Manifesto states a wide range of goals, but not in the clearest of language.
I believe political decentralisation must be a core goal,
because it's mentioned in the third sentence of the forum thread's opening post,
and frequently in the beginning of the manifesto as well:

<img src="./freq.svg" width=60%/>

The Manifesto starts with three pages of discussion regarding political (de)centralisation.
This discussion ends with the following two sentences near the middle of page 3:

_"Thus, in order to avoid falling pray to these arbitrary and trivial social factors which open the protocol up to
fundamentally centralising forces, we must take explicit action. The Polkadot Fellowship aims to be one example of such
an explicit action."_

After these words, the topic of (de)centralisation is barely mentioned again.
Instead the manifesto goes into detail about how the Fellowship will function.

## Hypothesis

My gut feeling was that, the Fellowship that's described after page 3 in the manifesto, as well as in the forum thread,
risk becoming heavily politically centralised, maybe even more than the Technical Committee and the company Parity are today.

## Method

To test this and facilitate objective discussion, I coded a simple simulation of how
the voting power of the seed group might develop compared to the collective voting power of everyone else.

The simulation contains a lot of guessed and approximated parameters, and probably bugs as well.
I encourage you to take a look at the code and adjust details within it to find simulation results that you find more probable/accurate than mine.
I also encourage you to ask why I initialized parameters the way I did.

Anyways, here's how I proceeded.

The distribution of voting power (called weight in the Manifesto) is equal to the distribution of formal power within the Fellowship,
since The Fellowship will make all its decisions through voting among members.

The ranks are numbers from 0 to 9.
Each member's voting power is computed as the triangle number of their rank.
This is described on page 6 of the Manifesto.

So voting power grows non-linearly with increasing rank, like this:

<img src="./one_fellow_voting_power.svg" width=60%/>

The maximum voting power is 45.

I looked up the 37 seed members of the Fellowship ([source](https://github.com/polkadot-fellows/seeding/blob/40c89dedae568a6c76872f268bd05e8497c8fa55/README.md)), and guesstimated their future voting power.

Governance is most interesting when there's a controversial referendum where a significant number of people
opposes the leaders' proposed solutions.

I therefore also modelled how many new core members might show up per year (set at 15)
and how their voting power will likely grow.
This lets us compare their power over time.

## Result

<img src="./plot_w_better_data.svg" width=60%/>

We see that if the Parity employees in the seed group sticks together and agrees,
they might dominate governance for ~13 years in my modelled scenario.

If all but the top 3 seed-members are commented out and never join the Fellowship,
or are in disagreement about the controversial subject so that their votes cancel out,
the top 3 alone can hold out against the non-seeds and dominate any vote for ~7 years.

<img src="./three_top_guns_scenario.svg" width=60%/>

Having all but the top 3 seed-members voting power nullified in the simulation makes sense because
there's an
explicit expectation/requirement that lower ranks vote in line with higher ranks. The Manifesto says:

_"Specifically, at rank three, this threshold is 100%—namely, of the decisions made where the members of rank four
and higher were in complete agreement, we expect any given member of rank three voted the same way. At rank four,
this reduces to 90% (and thus members of rank four need only vote with their superiors 90% of the time). At rank five,
this is 80% and rank six it is 70%."_

This puts a lot of pressure on lower ranked members to fall into line with higher ranked members if they want to survive/climb the organization.

Another factor that puts pressure on lower rank members is the knowledge that subjective opinion is unavoidable in the
process of recognizing their work. Higher ranks will form a subjective opinion and decide whether to promote or demote them.
The Manifesto touches upon this topic:

_"Recognition is underpinned by evaluation and recording. While blockchain systems make the act of recording fairly
trivial, evaluation must in large part be submitted on-chain through the judgements of existing members. Ensuring those
judgements reflect reality is crucial as inaccurate judgements can entirely undermine the social framework.
Providing convention on how to evaluate, decentralising the judgement and allowing anonymity to members are all means of
encouraging objectivity and accuracy."_

Recognition is a temporary social idea, quite random and constantly changing,
as discovered by those artists, mathematicians, and scientists throughout history
who happened to be ahead of their time.
Anonymity or on-chain recordings will of course not fundamentally change this basic fact.

The idea that top ranked members are able to reflect reality through their own uncoded judgement process is flawed,
so subjective opinion will come into play and put further pressure on lower ranked member to fall into line.

More pressure is added through a special rank-shift rule.
All ranks in Membership/Promotion/Continuation Votes are shifted downwards by 1, so rank 1 members loose their one vote.
This is described in the Manifesto's Eq 2 on page 6.
The following figure shows the top 3 likely initial members' potential dominance
of Membership/Promotion/Continuation Votes for the first ~12 years.

<img src="./plot_m.svg" width=60%/>

All in all, the Fellowship simulator paints an uniquely centralised power structure,
possibly more centralised than most of us (programmers) are used to from our more traditional teams,
informal working groups, and workplaces.

## Discussion

Some power factors are not covered in my analysis.
For example, if the top fellows have a large amount of informal power of all sorts.
Who owns which tokens, and power associated with that, are also not covered.

I think it looks like the Fellowship might stay very centralised for a very long time.
The only way to topple the top three would be to gather a backing from the majority of DOTs and
change the Fellowship's role in a general referendum.

A large amount of the Fellowship's centralisation comes from the checks-and-balances idea breaking down.
The idea is that other powerful groups exist, that can come to the rescue if the Fellowship becomes dysfunctional.
Examples of such groups could have been DOT holders, Parity, or the Council, but all those groups are led by
or strongly influenced by the Fellowship's top 3.

I am not trying to judge whether Fellowship decentralisation is good or bad for Polkadot.
Such a question has no well-defined answer.

I am trying to point out that Gavin sounds like he's aiming for political
decentralisation within technical leadership through the Fellowship,
and that I think he might risk unintentionally missing his own mark due to details in the Fellowship's design.

Bootstrapping and maintaining the Fellowship structure will require work, so we should convince ourselves
that it will be worthwhile, and that goals will be reached.
Uncovering flaws now will be cheaper than taking a relaxed position now and deal with flaws later.
Power structures are by nature hard to change for anyone but those on top.

If you read all the way down here you hopefully want to question my results, or the Fellowship's design, or both.
Please look into the file `sim/src/bin/members.rs` to investigate where my assumptions and guesses might have lead me to inaccurate results.
I compile and view simulation results like this

```bash
$ cd sim
$ cargo build && ./target/debug/members > plot.svg && eog plot.svg
```

## Disclaimer

I own some DOT. Nothing I write is financial advice. I don't have any insider information.
