fn main() {
    //let x: Vec<u32> = (0..=9).collect();
    //let triangle_numbers: Vec<u32> = (0..=9).map(|n| (0..=n).sum()).collect();
    let data = vec![
        [0, 0],
        [1, 1],
        [2, 3],
        [3, 6],
        [4, 10],
        [5, 15],
        [6, 21],
        [7, 28],
        [8, 36],
        [9, 45],
    ];
    let a = poloto::build::plot("").line().cloned(data.iter());
    poloto::data(a)
        .build_and_label(("", "Rank", "Voting Power"))
        .append_to(poloto::header().light_theme())
        .render_stdout();
}
