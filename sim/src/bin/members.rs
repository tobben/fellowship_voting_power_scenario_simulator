use poloto::prelude::*;
use rand::Rng;
use std::cmp::max;

#[derive(Debug, Clone)]
#[allow(dead_code)]
struct Member<'a> {
    name: &'a str,
    rank: i128, // poloto wants i128...
    years_from_i: u32,
    years_at_0: u32,
}

impl<'a> Member<'a> {
    const BASE_PROBABILITY_OF_PROMOTION: f64 = 0.5;
    const PROBABILITY_OF_DEMOTION: f64 = 0.5;
    const MAX_NUMBER_OF_CORE_COMPONENTS: f64 = 200f64;
    const M: i128 = 1;

    fn voting_power(&self) -> i128 {
        if self.rank > Member::M {
            (0..=(self.rank - Member::M)).sum()
        } else {
            0
        }
    }

    fn promotion_possible(&self) -> bool {
        (self.rank == 8 && self.years_from_i > 19)
            || (self.rank == 7 && self.years_from_i > 11)
            || (self.rank == 6 && self.years_from_i > 6)
            || (self.rank == 5 && self.years_from_i > 5)
            || (self.rank == 4 && self.years_from_i > 4)
            || (self.rank == 3 && self.years_from_i > 3)
            || (self.rank == 2 && self.years_from_i >= 2)
            || (self.rank == 1 && self.years_from_i >= 1)
            || self.rank == 0
    }

    fn demotion_possible(&self) -> bool {
        self.rank < 7
    }

    fn maybe_change_rank(&mut self, probability_of_promotion_above_3: f64) -> bool {
        let mut rng = rand::thread_rng();
        let rank_before = self.rank;
        let prob_promotion = if self.rank >= 3 {
            probability_of_promotion_above_3
        } else {
            Member::BASE_PROBABILITY_OF_PROMOTION
        };
        if self.promotion_possible() && rng.gen::<f64>() < prob_promotion {
            self.rank += 1;
        }
        if self.demotion_possible() && rng.gen::<f64>() < Member::PROBABILITY_OF_DEMOTION {
            self.rank = max(self.rank - 1, 0);
        }
        self.rank != rank_before
    }
}

fn member(name: &str, rank: i128, years_from_i: u32) -> Member {
    Member {
        name,
        rank,
        years_from_i,
        years_at_0: 0,
    }
}

fn main() {
    let initial_seed_members_parity: Vec<Member> = vec![
        member("Gavin Wood", 7, 7),        // 1 Parity
        member("Robert Habermeier", 6, 5), // 2 Parity
        member("Bastian Köcher", 6, 4),   // 3 Parity
                                           //member("André Silva", 5, 4),       // 4 Parity
                                           //member("Arkadiy Paronyan", 5, 7),   // 5 Parity
                                           //member("Sergey Pepyakin", 5, 5),    // 6 Parity
                                           //member("Jaco Greeff", 5, 5),        // 7 Parity
                                           //member("Shawn Tabrizi", 4, 4),      // 8 Parity
                                           //member("Kian Paimani", 4, 4),       // 9 Parity
                                           //member("Ordian Ardonik", 3, 2),     // 11 Parity
                                           //member("Nikolay Volf", 3, 3),       // 13 Parity and Gear
                                           //member("Alexander Theißen", 3, 3), // 14 Parity
                                           //member("Andrew Jones", 2, 3),       // 15 Parity
                                           //member("Joe Petrowski", 2, 4),      // 16 Parity
                                           //member("Keith Yeung", 2, 2),        // 17 Parity
                                           //member("Jan (koute)", 2, 1),        // 19 Parity
                                           //member("Giles Cope", 1, 1),         // 23 Parity
                                           //member("Oliver Tale-Yazdi", 1, 1),  // 24 Parity
                                           //member("Jesse Chejieh", 1, 1),      // 28 Parity and Polytope Labs
    ];

    let initial_other_members: Vec<Member> = vec![
        //member("Xiliang (Bryan) Chen", 4, 4),    // 10 Acala and Laminar
        //member("Bernhard Schuster", 3, 2),       // 12 Earlier parity, now Gensyn
        //member("Shaun Wang", 2, 1),              // 18 Acala and Laminar
        //member("Seun Ianlege", 2, 2),            // 20 Earlier Parity, now ComposableFi
        //member("Xavier Lau", 1, 2),              // 21 Darwinia Network
        //member("Aleksandr Krupenkin", 1, 3),     // 22 Astar
        //member("Eclesio Melo", 1, 1),            // 25 Gossamer/ChainSafe
        //member("Amar Singh", 1, 1),              // 26 Earlier Parity, now Moonbeam
        //member("Daniel Olano", 1, 3),            // 27 W3F and treasury funded projects
        //member("Quentin McGaw", 1, 1),           // 29 Gossamer/ChainSafe
        //member("Charlie Ferrell", 1, 1),         // 30 Acala
        //member("Andrew Plaza", 1, 2),            // 31 Earlier Parity, now Enjin
        //member("Gabriel Facco de Arruda", 1, 0), // 32 InvArch
        //member("Thibaut Sardan", 1, 3),          // 33 Earlier Parity, now ChainSafe
        //member("Sergej Sakac", 1, 1),            // 34 Student
        //member("Kishan Sagathiya", 1, 1),        // 35 Gossamer/ChainSafe
        //member("Damilare Akinlose", 1, 1),       // 36 Polytope Labs/Commonwealth Labs
        //member("David Salami", 1, 1),            // 37 Composable Finance/Commonwealth Labs
    ];

    // It's not allowed for new members to oppose the higher ranks (seed group).
    // Modelling that 15 non-seeders start doing so per year anyways is unrealistic,
    // but is done to make the simulation interesting at all.
    let new_others_per_year = 15;
    let num_simulations = 100;
    let num_years = 30;
    let mut combined_seed_member_data: Vec<[i128; 2]> = vec![];
    let mut combined_other_member_data: Vec<[i128; 2]> = vec![];
    for sim in 0..num_simulations {
        let mut seed_members: Vec<Member> = initial_seed_members_parity.clone();
        let mut other_members: Vec<Member> = initial_other_members.clone();
        let mut seed_member_data: Vec<[i128; 2]> = vec![];
        let mut other_member_data: Vec<[i128; 2]> = vec![];
        for year in 0..=num_years {
            let combined_seed_member_voting_power =
                seed_members.iter().map(|m| m.voting_power()).sum();
            seed_member_data.push([year, combined_seed_member_voting_power]);
            let combined_other_member_voting_power =
                other_members.iter().map(|m| m.voting_power()).sum();
            other_member_data.push([year, combined_other_member_voting_power]);

            // Above rank 3 members are proposing, designing and realising core components
            let already_invented_core_components =
                (seed_members
                    .iter()
                    .fold(0, |acc, m| if m.rank >= 4 { acc + m.rank - 3 } else { acc })
                    + other_members.iter().fold(0, |acc, m| {
                        if m.rank >= 4 {
                            acc + m.rank - 3
                        } else {
                            acc
                        }
                    })) as f64;
            let probability_above_3 = Member::BASE_PROBABILITY_OF_PROMOTION
                * (1f64 - already_invented_core_components / Member::MAX_NUMBER_OF_CORE_COMPONENTS);

            for m in &mut seed_members {
                if m.rank > 0 {
                    m.years_from_i += 1
                }
                let rank_changed = m.maybe_change_rank(probability_above_3);
                if m.rank == 0 && !rank_changed {
                    m.years_at_0 += 1;
                }
            }

            for _ in 0..new_others_per_year {
                other_members.push(member("anon", 0, 0))
            }
            for m in &mut other_members {
                if m.rank > 0 {
                    m.years_from_i += 1
                }
                let rank_changed = m.maybe_change_rank(probability_above_3);
                if m.rank == 0 && !rank_changed {
                    m.years_at_0 += 1;
                }
            }
            other_members.retain(|m| m.years_at_0 < 2);
        }
        if sim == 0 {
            combined_seed_member_data = vec![[0, 0]]; // Draw seed as sudden jump from 0
            combined_seed_member_data.append(&mut seed_member_data.clone());
            combined_other_member_data = vec![[0, 0]]; // Draw seed as sudden jump from 0
            combined_other_member_data.append(&mut other_member_data.clone());
        } else {
            for year in 0..seed_member_data.len() {
                combined_seed_member_data[year + 1][1] += seed_member_data[year][1];
            }
            for year in 0..other_member_data.len() {
                combined_other_member_data[year + 1][1] += other_member_data[year][1];
            }
        }
    }
    for years_data in &mut combined_seed_member_data {
        years_data[1] /= num_simulations;
    }
    for years_data in &mut combined_other_member_data {
        years_data[1] /= num_simulations;
    }

    let a = poloto::build::plot("Top 3")
        .line()
        .cloned(combined_seed_member_data.iter());
    let b = poloto::build::plot("Non-parity")
        .line()
        .cloned(combined_other_member_data.iter());

    poloto::data(plots!(a, b))
        .build_and_label((
            format!("Rank shift = 1"),
            "Years after 2022",
            "Voting Power",
        ))
        .append_to(poloto::header().light_theme())
        .render_stdout();
}
